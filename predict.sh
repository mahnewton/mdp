#!/bin/bash

while read id; do
    echo $id
    python ./scripts/mdp.py -d ./input/$id -o ./output  -t B    # A: Ca-Ca distance prediction; B = Cb-Cb distance prediction;
done < 'protein_list.lst'
