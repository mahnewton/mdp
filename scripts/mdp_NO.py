#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#%% Env check 
import sys
if sys.version_info < (3,0,0):
    print('Python 3 required!!!')
    sys.exit(1)

#%% parse input-output options
import os
import getopt


def show_usage_info():
    print('Usage:' + sys.argv[0] + ' -d data_directory -o output_directory')


try:
    opts, args = getopt.getopt(sys.argv[1:], "d:o:h")
except getopt.GetoptError as err:
    print(err)
    show_usage_info()
    sys.exit(2)

for opt, opt_val in opts:
    if opt in ("-h", "--help"):
        show_usage_info()
        sys.exit()
    elif opt in ("-d"):
        data_dir = os.path.abspath(opt_val)    
    elif opt in ("-o"):
        output_dir = os.path.abspath(opt_val)     
    else:
        print('Unhandled option provided.')
        show_usage_info()
        sys.exit(1)

def is_all_data_exists(data_dir):
    pid = data_dir.split('/')[-1]
    fasta_file = os.path.join(data_dir, pid) + '.fasta'
    ccmpred_file = os.path.join(data_dir, pid) + '.ccmpred'
    colstats_file = os.path.join(data_dir, pid) + '.colstats'
    contact_potential_file = os.path.join(data_dir, pid) + '.pairstats'
    pssm_file = os.path.join(data_dir, pid) + '.pssm.npy'
    freecontact_file = os.path.join(data_dir, pid) + '.freecontact.rr'
    pcp_file = os.path.join(data_dir, pid) + '.pcp.npy'
    
    if os.path.exists(fasta_file) and os.path.exists(ccmpred_file) and os.path.exists(colstats_file) and os.path.exists(pssm_file) and os.path.exists(freecontact_file) and os.path.exists(pcp_file):
        return True
    else:
        print("One or more data file missing. Check all files present in the target protein's directory.")
        print("Target protein's data directory: " + data_dir)
        return False


if not is_all_data_exists(data_dir):
    exit(1)
if not os.path.exists(output_dir):
    print('Output path not exists at: ' + output_dir)
    exit(1)

#%% imports
from Bio import SeqIO

import numpy as np
import tensorflow as tf
from tensorflow.keras.layers import Input, Convolution2D, Activation, add, Dropout, BatchNormalization
from tensorflow.keras.models import Model
epsilon = tf.keras.backend.epsilon()


#%% 
def get_model(seq_length, n_res_blocks, expected_n_channels, weight):
       
    # model params
    L = seq_length
    n_filters = 64
    dropout_rate = 0.2

    # define model architechture, part adapted from pdnet
    my_input = Input(shape = (L, L, expected_n_channels))
    tower = BatchNormalization()(my_input)
    tower = Activation('relu')(tower)
    tower = Convolution2D(n_filters, 1, padding = 'same')(tower)
    
    flag_1D = False
    d_rate = 1
    for i in range(n_res_blocks):
        block = BatchNormalization()(tower)
        block = Activation('elu')(block) 
        if flag_1D:
            block = Convolution2D(n_filters, kernel_size = (1, 5), padding = 'same', kernel_initializer="he_normal")(block)
        else:
            block = Convolution2D(n_filters, kernel_size = (3, 3), padding = 'same', kernel_initializer="he_normal")(block)
        
        block = Dropout(dropout_rate)(block)
        block = Activation('elu')(block)  # elu
        
        if flag_1D:
            block = Convolution2D(n_filters, kernel_size = (1, 5), dilation_rate=(d_rate, d_rate), padding = 'same', kernel_initializer="he_normal")(block)
            flag_1D = False
        else:
            block = Convolution2D(n_filters, kernel_size = (3, 3), dilation_rate=(d_rate, d_rate), padding = 'same', kernel_initializer="he_normal")(block)
            flag_1D = True
            
        tower = add([block, tower])
        if d_rate == 1:
            d_rate = 2
        elif d_rate == 2:
            d_rate = 4
        else:
            d_rate = 1
        
    tower = BatchNormalization()(tower)
    tower = Activation('relu')(tower)
    tower = Convolution2D(1, 3, padding = 'same')(tower)
    tower = Activation('relu')(tower)
    
    model = Model(my_input, tower)
    model.load_weights(weight)   
    return model
#%%
def get_s_features(data_dir, pid, expected_n_channels):
    
    X = np.full((seq_length, seq_length, expected_n_channels), 0.0)
    f_channels = 0
    
    # Add CCMpred
    ccmpred_file = os.path.join(data_dir, pid) + '.ccmpred'
    with open(ccmpred_file) as f:
        ccmlist = [[float(num) for num in line.strip().split()] for line in f ]
        ccmpred = np.array(ccmlist, dtype = np.float16)
        assert ccmpred.shape == ((seq_length, seq_length))
        X[:, :, f_channels] = ccmpred
        f_channels += 1
        
    # Add FreeContact
    freecontact_file = os.path.join(data_dir, pid) + '.freecontact.rr'
    freecontact = np.zeros((seq_length, seq_length))
    with open(freecontact_file) as f:    
        for l in f.readlines():
            c = l.strip().split()
            freecontact[int(c[0]) - 1, int(c[2]) - 1] = float(c[5])
            freecontact[int(c[2]) - 1, int(c[0]) - 1] = float(c[5])
        
        X[:, :, f_channels] = freecontact.astype(np.float16)
        f_channels += 1
        
    # ### Add contact potential, it is a lxl matrix, and n_channel = 1
    contactpotential_file = os.path.join(data_dir, pid) + '.pairstats'
    potential = np.zeros((seq_length, seq_length))
    with open(contactpotential_file) as f:
        for l in f.readlines():
            c = l.strip().split()
            potential[int(c[0]) - 1, int(c[1]) - 1] = float(c[2])
            potential[int(c[1]) - 1, int(c[0]) - 1] = float(c[2])
            assert potential.shape == ((seq_length, seq_length))
        X[:, :, f_channels] = potential.astype(np.float16)
        f_channels += 1

    # Add PSSM
    pssm_file = os.path.join(data_dir, pid) + '.pssm.npy'
    pssm = np.load(pssm_file)
    assert pssm.shape == (seq_length, 22)
    for j in range(22):
        a = np.repeat(pssm[:, j].reshape(1, seq_length), seq_length, axis = 0)
        X[:, :, f_channels] = a
        f_channels += 1
        X[:, :, f_channels] = a.T
        f_channels += 1
    
    ## Add 7 Physicochemical encoding
    pcp_file = os.path.join(data_dir, pid) + '.pcp.npy'
    pcp = np.load(pcp_file)
    assert pcp.shape == (seq_length,7)
    for j in range(7):
        a = np.repeat(pcp[:,j].reshape(1, seq_length), seq_length, axis = 0)
        X[:, :, f_channels] = a
        f_channels += 1
        X[:, :, f_channels] = a.T
        f_channels += 1
    
    # Add Shanon entropy
    entropy = []
    colstats_file = os.path.join(data_dir, pid) + '.colstats'
    with open(colstats_file) as f:  
        f.readline()
        f.readline()
        f.readline()
        f.readline()
        for l in f.readlines():
            c = l.strip().split()
            entropy.append(c[21])
        s_entropy = np.array(entropy, dtype = np.float16)
        assert s_entropy.shape == (seq_length, )
        a = np.repeat(s_entropy.reshape(1, seq_length), seq_length, axis = 0)
        X[:, :, f_channels] = a
        f_channels += 1
        X[:, :, f_channels] = a.T
        f_channels += 1

   
    assert len(X[0, 0, :]) == expected_n_channels
    
    XX = np.full((1, OUTL, OUTL, expected_n_channels), 0.0)
    Xpadded = np.zeros((seq_length + pad_size, seq_length + pad_size, len(X[0, 0, :])))
    Xpadded[int(pad_size/2) : seq_length+int(pad_size/2), int(pad_size/2) : seq_length+int(pad_size/2), :] = X
    length = len(Xpadded[:, 0, 0])
    XX[0, :length, :length, :] = Xpadded
    
    return XX


def get_e_features(predicted_s, predicted_l, predicted_w, pid, expected_e_channels):
    
    X = np.full((seq_length, seq_length, expected_e_channels), 0.0)
       
    X[:, :, 0] = predicted_s # dist2to20
    X[:, :, 1] = predicted_l # dist16to36
    X[:, :, 2] = predicted_w # dist_2to36
           
    assert len(X[0, 0, :]) == expected_e_channels
    
    XX = np.full((1, OUTL, OUTL, expected_e_channels), 0.0)
    Xpadded = np.zeros((seq_length + pad_size, seq_length + pad_size, len(X[0, 0, :])))
    Xpadded[int(pad_size/2) : seq_length+int(pad_size/2), int(pad_size/2) : seq_length+int(pad_size/2), :] = X
    length = len(Xpadded[:, 0, 0])
    XX[0, :length, :length, :] = Xpadded
    
    return XX

def post_processing(predicted_dists, OUTL, pad_size, seq_length):
    # Remove padding, i.e. shift up and left by int(pad_size/2)
    predicted_dists[:, :(OUTL-pad_size), :(OUTL-pad_size), :] = predicted_dists[:, int(pad_size/2) : OUTL-int(pad_size/2), int(pad_size/2) : OUTL-int(pad_size/2), :]
        
    predicted_dists = predicted_dists[0,:seq_length,:seq_length,0]
    return predicted_dists

def final_processing(predicted_dists, OUTL, pad_size, seq_length):
    # Remove padding, i.e. shift up and left by int(pad_size/2)
    predicted_dists[:, :(OUTL-pad_size), :(OUTL-pad_size), :] = predicted_dists[:, int(pad_size/2) : OUTL-int(pad_size/2), int(pad_size/2) : OUTL-int(pad_size/2), :]
    predicted_dists[ predicted_dists < 0.02 ] = 0.02
    predicted_dists = 10.0 / ((predicted_dists ** 0.5) + epsilon)
        
    predicted_dists = predicted_dists[0,:seq_length,:seq_length,0]
    return predicted_dists

#%%
pid = data_dir.split('/')[-1]
fasta_file = os.path.join(data_dir, pid) + '.fasta'
print(fasta_file)
aa_seq_record = SeqIO.read(fasta_file, 'fasta')
aa_seq = str(aa_seq_record.seq)
seq_length = len(aa_seq)

#%% global vars
expected_s_channels = 63
expected_e_channels = 3
s_res_blocks = 128
e_res_blocks = 32
pad_size = 10
OUTL = seq_length + pad_size # for a particular sequence

#%%
XX_s = get_s_features(data_dir, pid, expected_s_channels)

# load pre-trained weights
weights_dir = './models/'
weight_dir = weights_dir+'NO_Atoms/'
weight_s = weight_dir+'NO_2to20.hdf5'
model_s = get_model(OUTL, s_res_blocks, expected_s_channels, weight_s)
predicted_s = model_s.predict(XX_s)
predicted_s = post_processing(predicted_s, OUTL, pad_size, seq_length)

weight_l = weight_dir+'NO_16to36.hdf5'
model_l = get_model(OUTL, s_res_blocks, expected_s_channels, weight_l)
predicted_l = model_l.predict(XX_s)
predicted_l = post_processing(predicted_l, OUTL, pad_size, seq_length)

weight_w = weight_dir+'NO_2to36.hdf5'
model_w = get_model(OUTL, s_res_blocks, expected_s_channels, weight_w)
predicted_w = model_w.predict(XX_s)
predicted_w = post_processing(predicted_w, OUTL, pad_size, seq_length)

XX_e = get_e_features(predicted_s, predicted_l, predicted_w, pid, expected_e_channels)
weight_e = weight_dir+'NO_ME_F64_32.hdf5'
model_e = get_model(OUTL, e_res_blocks, expected_e_channels, weight_e)
predicted_e = model_e.predict(XX_e)
    

predicted_dists = final_processing(predicted_e, OUTL, pad_size, seq_length)
np.save(output_dir +'/'+ pid +'_NO_dist.npy', predicted_dists)
print('N-O distance of '+pid+' predicted!!!!!!!!!!!')
